package tp6;

import static org.junit.Assert.*;
import org.junit.Test;

/*
 * Copyright (C) 2018 University of Lille
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Tests for a priority queue
 * @author <a href="mailto:Frederic.Guyomarch@univ-lille1.fr">Frédéric Guyomarch</a>, IUT-A
 * @date January 18th
 */

public class PriorityQueueTest {

	@Test
	public void testIsEmpty() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>();
		assertTrue(tas.isEmpty());
		tas.offer(20);
		assertFalse(tas.isEmpty());
	}

	@Test
	public void testMakeEmpty() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>();
		tas.offer(20);
		tas.clear();
		assertTrue(tas.isEmpty());
	}

	@Test
	public void testToString() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>();
		assertEquals(tas.toString(), "[]");
		tas.offer(14);
		tas.offer(10);
		tas.offer(16);
		assertEquals("[16, 10, 14]", tas.toString());
	}

	@Test                                 
	public void testOffer() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>(10);
		assertTrue(tas.offer(33));
		assertTrue(tas.offer(45));
		assertTrue(tas.offer(7));
		assertTrue(tas.offer(20));
		assertTrue(tas.offer(10));
		assertTrue(tas.offer(50));
		assertEquals("[50, 33, 45, 20, 10, 7]", tas.toString());
	}

	@Test                                 
	public void testOfferFull() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>(4);
		assertTrue(tas.offer(33));
		assertTrue(tas.offer(45));
		assertTrue(tas.offer(7));
		assertTrue(tas.offer(20));
		assertFalse(tas.offer(10));
		assertFalse(tas.offer(50));
		assertEquals("[45, 33, 7, 20]", tas.toString());
	}

	@Test
	public void testSize() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>();
		assertEquals(0, tas.size());
		tas.offer(33);
		tas.offer(45);
		tas.offer(7);
		assertEquals(3, tas.size());
		tas.offer(17);
		tas.offer(1);
		assertEquals(5, tas.size());
	}

	@Test
	public void testPoll() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>();
		assertEquals(null, tas.poll());
		tas.offer(5);
		tas.offer(4);
		tas.offer(33);
		assertEquals(3, tas.size());
		assertEquals(Integer.valueOf(33), tas.poll());
		assertEquals(2, tas.size());
		assertEquals(Integer.valueOf(5), tas.poll());
		assertEquals(1, tas.size());
		assertEquals(Integer.valueOf(4), tas.poll());
		assertTrue(tas.isEmpty());
	}

	@Test
	public void testPeek() {
		HeapPriorityQueue<Integer> tas = new HeapPriorityQueue<>();
		tas.offer(5);
		tas.offer(4);
		tas.offer(33);
		assertEquals(Integer.valueOf(33), tas.peek());
		tas.offer(34);
		assertEquals(Integer.valueOf(34), tas.peek());
		tas.offer(28);
		assertEquals(Integer.valueOf(34), tas.peek());
	}
}

