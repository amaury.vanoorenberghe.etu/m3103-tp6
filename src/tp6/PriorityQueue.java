package tp6;

import java.util.Comparator;

/*
 * Copyright (C) 2018 University of Lille
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Interface for a priority queue
 * @author from JAVA API
 * @date January 18th
 */
public interface PriorityQueue<E> {
    /** Removes all of the elements from this priority queue. */
    void clear();
	
    /**Returns the comparator used to order the elements in this queue, or null if this queue is sorted according to the natural ordering of its elements. */
    Comparator<? super E> comparator();

    /** Inserts the specified element into this priority queue. */
    boolean offer(E e);

    /** Retrieves, but does not remove, the head of this queue, or returns null if this queue is empty. */
    E peek();

    /** Retrieves and removes the head of this queue, or returns null if this queue is empty. */
    E poll();

    /** Returns the number of elements in this collection. */
    int size();

    /** Returns <code>true</code> if this collection contains no elements. */
    boolean isEmpty();
}