package tp6;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class HeapPriorityQueue<E> implements PriorityQueue<E> {
	public static int DEFAULT_CAPACITY = 1024;
	private final Comparator<? super E> COMPARATOR;
	private E[] heap;
	private int currentSize = 0;

	public HeapPriorityQueue() {
		this(DEFAULT_CAPACITY);
	}

	public HeapPriorityQueue(Comparator<? super E> comparator) {
		this(DEFAULT_CAPACITY, comparator);
	}

	public HeapPriorityQueue(int capacity) {
		this(capacity, null);
	}

	@SuppressWarnings("unchecked")
	public HeapPriorityQueue(int capacity, Comparator<? super E> comparator) {
		heap = (E[]) new Object[capacity];
		COMPARATOR = comparator;
	}

	@Override
	public void clear() {
		Arrays.fill(heap, null);
		currentSize = 0;
	}

	@Override
	public Comparator<? super E> comparator() {
		return COMPARATOR;
	}

	@Override
	public boolean offer(E e) {
		if (currentSize >= heap.length) {
			return false;
		}
		
		int idx = currentSize, parent = idxParentNode(idx);
		heap[idx] = e;
		
		while (idx > 0 && compare(heap[parent], heap[idx]) < 0) {
			swap(idx, parent);
			idx = parent;
			parent = idxParentNode(idx);
		}
		
		++currentSize;
		return true;
	}

	@Override
	public E peek() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		return heap[0];
	}

	@Override
	public E poll() {
		if (currentSize <= 0) {
			return null;
		}

		final E tmp = peek();

		final int last = idxLast();
		swap(0, last);
		--currentSize;

		int idx = 0;
		int child = idxMaxChild(idx);
		while (child < currentSize && compare(heap[idx], heap[child]) < 0) {
			swap(child, idx);

			idx = child;
			child = idxMaxChild(idx);
		}

		return tmp;
	}

	@Override
	public int size() {
		return currentSize;
	}

	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}

	// ===== UTIL =====

	protected int idxParentNode(int i) {
		if (i <= 0) {
			return -1;
		}

		return (i - 1) / 2;
	}

	protected int idxMaxChild(int i) {
		final int idxL = idxLeftChild(i);
		final int idxR = idxRightChild(i);

		return idxR >= currentSize || compare(heap[idxL], heap[idxR]) > 0 ? idxL : idxR;
	}

	protected int idxLeftChild(int i) {
		return 2 * i + 1;
	}

	protected int idxRightChild(int i) {
		return 2 * i + 2;
	}

	protected int idxLast() {
		return currentSize - 1;
	}

	protected void swap(int a, int b) {
		final E tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;
	}

	@SuppressWarnings("unchecked")
	protected int compare(E a, E b) {
		try {
			return ((Comparable<E>) a).compareTo(b);
		} catch (ClassCastException e) {
			if (COMPARATOR != null) {
				return COMPARATOR.compare(a, b);
			} else {
				throw new IllegalStateException();
			}
		}
	}

	@Override
	public String toString() {
		return 
		"[" + Arrays
		.stream(heap)
		.filter(item -> item != null)
		.map(item -> item.toString())
		.collect(Collectors.joining(", "))
		+ "]";
	}
}